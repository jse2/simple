package domain;

public abstract class Shape {

    public abstract double getArea();

}