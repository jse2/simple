import domain.Shape;

import java.util.Collection;
import java.util.LinkedList;

import domain.*;

public class Main {

    public static void main(String[] args) {
        Collection<Circle> circles = new LinkedList<>();
        addShape(circles, new Circle(4), new Circle(5));
        System.out.println("Circles area is : " + getSumArea(circles));

        Collection<Rectangle> rectangles = new LinkedList<>();
        addShape(rectangles, new Rectangle(4, 5), new Rectangle(6, 7));
        System.out.println("Rectangles area is : " + getSumArea(rectangles));

        Collection<Square> squares = new LinkedList<>();
        addShape(squares, new Square(4), new Square(5));
        System.out.println("Squares area is : " + getSumArea(squares));

        Collection<Shape> shapes = new LinkedList<>();
        addShape(shapes, new Circle(4), new Square(5), new Rectangle(4, 5));
        System.out.println("Shapes area is : " + getSumArea(shapes));

        Collection<Shape> empty = new LinkedList<>();
        System.out.println("Empty area is : " + getSumArea(empty));
    }

    public static <T extends Shape> void addShape(Collection<T> collection, T...shapes) {
        for (T shape : shapes) {
            collection.add(shape);
        }
    }

    public static <T extends Shape> double getSumArea(Collection<T> shapes) {
        return shapes.stream().mapToDouble(Shape::getArea).sum();
    }

}
